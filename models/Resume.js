const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// const Skill = require("./Skill");

const ResumeSchema = new Schema({
    owner: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    skills: [{
        name: {
            type: String,
            required: true
        },
        experience: {
            type: Number
        }
    }],
    experience: {
        type: Number
    },
    starred: {
        type: Boolean
    },
    blacklisted: {
        type: Boolean
    },
    tags: {
        type: [String]
    },
    notes: {
        type: [String]
    },
    issues: {
        type: [String]
    }
});

const Resume = mongoose.model("resume", ResumeSchema);

module.exports = Resume;