const regexifier = require("../utils/regexifier");

const Skill = require("../models/Skill");

module.exports = {
    create(req, res, next) {
        const skillProps = req.body;

        Skill.create(skillProps)
            .then(skill => res.send(skill))
            .catch(next);
    },

    edit(req, res, next) {
        const skillId = req.params.id;
        const skillProps = req.body;

        Skill.findByIdAndUpdate({
                _id: skillId
            }, skillProps)
            .then(() => Skill.findById({
                _id: id
            }))
            .then(skill => res.send(skill))
            .catch(next);
    },

    delete(req, res, next) {
        const skillId = req.params.id;

        // console.log(skillId);
        Skill.findByIdAndRemove({
                _id: skillId
            })
            .then(skill => {
                // console.log(skill);
                res.status(204).send(skill)
            })
            .catch(next);
    },

    search(req, res, next) {
        const term = new RegExp(regexifier.regexify(req.body.term), "i");
        // console.log(term);

        Skill.find({
            name: {
                $regex: term
            }
        }).then(skills => {
            res.send(skills);
        }).catch(next);
    }
};