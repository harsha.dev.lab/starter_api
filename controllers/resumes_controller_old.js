const regexifier = require("../utils/regexifier");

const Resume = require("../models/Resume");

module.exports = {
    create(req, res, next) {
        const resumeProps = req.body;

        Resume.create(resumeProps)
            .then(resume => res.send(resume))
            .catch(next);
    },

    edit(req, res, next) {
        const resumeId = req.params.id;
        const resumeProps = req.body;

        Resume.findByIdAndUpdate({
                _id: resumeId
            }, resumeProps)
            .then(() => Resume.findById({
                _id: id
            }))
            .then(resume => res.send(resume))
            .catch(next);
    },

    delete(req, res, next) {
        const resumeId = req.params.id;

        Resume.findByIdAndRemove({
                _id: resumeId
            })
            .then(resume => res.status(204).send(resume))
            .catch(next);
    },

    search(req, res, next) {
        const searchProps = req.body;
        const {
            owner,
            terms,
            blacklisted,
            starred,
            experience
        } = searchProps;

        // console.log("SEARCHING");

        function regexify(array) {
            return array.map((term) => {
                return new RegExp(regexifier.regexify(term), "i");
            });
        };

        function genOrTermsQuery(array) {
            return array.map((term) => {
                return {
                    $or: [{
                            text: {
                                $regex: term
                            }
                        },
                        {
                            tags: {
                                $regex: term
                            }
                        },
                        {
                            notes: {
                                $regex: term
                            }
                        },
                        {
                            "skills.name": {
                                $regex: term
                            }
                        }
                    ]
                };
            });
        };

        function genAndQuery(array) {
            return {
                $and: array
            };
        };

        function addExperienceQuery(q) {
            q.$and.push({
                experience: {
                    $gte: experience.from,
                    $lte: experience.to
                }
            });
        };

        const orTermsQuery = genOrTermsQuery(regexify(terms));
        const query = genAndQuery(orTermsQuery);
        addExperienceQuery(query);

        console.log(query);

        Resume.find(query)
            .then(resume => {
                res.send(resume);
            })
            .catch(next);
    }
};