const User = require("../models/User");

module.exports = {
  index(req, res, next) {
    const { lng, lat } = req.query;
    const point = {
      type: "Point",
      coordinates: [parseFloat(lng), parseFloat(lat)]
    };

    User.aggregate([
      {
        $geoNear: {
          near: point,
          spherical: true,
          maxDistance: 200000,
          distanceField: "dist.calculated"
        }
      }
    ])
      .then(users => res.send(users))
      .catch(next);
  },

  create(req, res, next) {
    const userProps = req.body;

    User.create(userProps)
      .then(user => res.send(user))
      .catch(next);
  },

  edit(req, res, next) {
    const userId = req.params.id;
    const userProps = req.body;

    User.findByIdAndUpdate({ _id: userId }, userProps)
      .then(() => User.findById({ _id: id }))
      .then(user => res.send(user))
      .catch(next);
  },

  delete(req, res, next) {
    const userId = req.params.id;

    User.findByIdAndRemove({ _id: userId })
      .then(user => res.status(204).send(user))
      .catch(next);
  },

  login(req, res, next) {
    const { username, password } = req.body;

    User.findOne({ username: username })
      .then(user => {
        user.comparePassword(password, (err, isMatch) => {
          try {
            if (err) {
              throw err;
            }

            if (isMatch) {
              res.send(user);
            } else {
              throw "wrong password";
            }
          } catch (err) {
            next({
              message: err
            });
          }
        });
      })
      .catch(next);
  }
};
