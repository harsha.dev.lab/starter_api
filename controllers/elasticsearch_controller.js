const elasticsearch = require("elasticsearch");

const client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace"
});

module.exports = {
    // test if elasticsearch is up
    test() {
        client.ping()
            .then(() => console.log("ES cluster is swell :)"))
            .catch((err) => console.log("ES cluster is down!", err));
    },

    // create index
    createIndex(name, mappings) {
        return new Promise((resolve, reject) => {
            client.indices.create({
                index: name,
                body: mappings
            }).then((response) => {
                resolve(response);
            }).catch((error) => {
                console.error("Error occurred in createIndex", error);
                reject(error);
            });
        });
    },

    // delete index
    deleteIndex(name) {
        return new Promise((resolve, reject) => {
            client.indices.delete({
                index: name
            }).then((response) => {
                resolve(response);
            }).catch((error) => {
                console.error("Error occurred in deleteIndex", error);
                reject(error);
            });
        });
    }
}