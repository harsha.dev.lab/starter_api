module.exports = {
    "properties": {
        "owner": {
            "type": "text"
        },
        "type": {
            "type": "text"
        },
        "name": {
            "type": "text",
            "analyzer": "whitespacelower"
        },
        "text": {
            "type": "text",
            "analyzer": "whitespacelower"
        },
        "skills": {
            "properties": {
                "name": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                }
            }
        },
        "notes": {
            "properties": {
                "text": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                }
            }
        },
        "tags": {
            "properties": {
                "name": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                }
            }
        },
        "reports": {
            "properties": {
                "name": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                },
                "text": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                }
            }
        },
        "sharedWith": {
            "properties": {
                "userid": {
                    "type": "text"
                },
                "username": {
                    "type": "text",
                    "analyzer": "whitespacelower"
                }
            }
        }
    }
};