const talentmapsettings = require("./talentmapsettings");
const docmapping = require("./docmapping");

module.exports = {
    "settings": talentmapsettings,
    "mappings": {
        "doc": docmapping
    }
};