module.exports = {
    // build search query
    build(opts) {
        const {
            terms,
            notTerms,
            blacklisted,
            starred,
            reported
        } = opts;
        const body = {
            query: {
                bool: {
                    must: this.genShouldTerms({
                        terms
                    })
                }
            }
        };

        return body;
    },

    // generate should terms for text, tags, and notes fields
    genShouldTerms({
        terms
    }) {
        return terms.map(term => {
            const boolObj = {
                bool: {
                    should: []
                }
            };
            const toks = term.split(" ");
            if (toks.length > 1) {
                boolObj.bool.should.push({
                    match_phrase: {
                        text: term
                    }
                });
                boolObj.bool.should.push({
                    match_phrase: {
                        tags: term
                    }
                });
                boolObj.bool.should.push({
                    match_phrase: {
                        notes: term
                    }
                });
            } else {
                boolObj.bool.should.push({
                    term: {
                        text: term
                    }
                });
                boolObj.bool.should.push({
                    term: {
                        tags: term
                    }
                });
                boolObj.bool.should.push({
                    term: {
                        notes: term
                    }
                });
            }

            return boolObj;
        });
    }
};