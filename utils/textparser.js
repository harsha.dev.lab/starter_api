const path = require("path");
const textract = require("textract");

module.exports = {
    extractText(fileloc){
        return new Promise((resolve, reject) => {
            const filename = fileloc.split("/").pop();

            let textractconfig = {
                preserveLineBreaks: true
            };

            textract.fromFileWithPath(fileloc, textractconfig, (err, text) => {
                if(err){
                    console.error(`Textract: ${err}`);
                    reject({
                        success: false,
                        error: err
                    });
                }

                resolve({
                    success: true,
                    text: text
                });
            });
        });
    }
}