const _ = require("lodash");

module.exports = {
    // months
    months: [
        "January", "Febuary", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"
    ],

    // month abbreviations
    monthAbbrs: [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun",
        "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec"
    ],

    // lowercase months
    lowercaseMonths: [
        "january", "febuary", "march",
        "april", "may", "june",
        "july", "august", "september",
        "october", "november", "december"
    ],

    // lowercase month abbreviations
    lowercaseMonthAbbrs: [
        "jan", "feb", "mar",
        "apr", "may", "jun",
        "jul", "aug", "sep",
        "oct", "nov", "dec"
    ],

    // strings representing the present
    presentStrs: [
        "Present",
        "Now"
    ],

    // named month regex
    // January 2001
    // Febuary 1999
    // March 2003
    // April 2000
    // May 1988
    // June 1960
    // July 1910
    // August 18
    // September 22
    // October 99
    // November 00
    // December 99
    // and all abbreviated versions
    namedMonthRegex: /(\b\w{3,9}[^a-zA-Z0-9]*((?:[1]|[2])(?:[0]|[9]))*(?:[0-9]|[6-9])[0-9]\b)/g,

    // numbered month regex
    // 06 2000
    // 09/2000
    // 12' /01
    // 1/-- 1955
    // 04   /13
    numberedMonthRegex: /(\b(?:[1-9]|[0-1][0-9])[^a-zA-Z0-9]*((?:[1]|[2])(?:[0]|[9]))*(?:[0-9]|[6-9])[0-9]\b)/g,

    // present regex
    presentRegex: /(?:Present|Ti[l]+[\']*[\s]*Date|Now)/gi,

    // checks if string is a month
    parseMonth(str) {
        const ret = {
            found: false,
            match: "",
            type: ""
        };

        // if date is present
        if (this.containsPresent(str)) {
            ret.found = true;
            ret.match = "Present";
            ret.type = "present";
            console.log("CONTAINS PRESENT", ret);
            return ret;
        }

        // if month comes in number format; ex: 03 is March
        if (!isNaN(str)) {
            const index = parseInt(str);
            if (index > 0 && index <= 12) {
                ret.found = true;
                ret.match = this.months[index - 1];
                ret.type = "number";
                return ret;
            }
        }

        // deburr removes accents
        const tstr = _.deburr(str.toLowerCase());

        for (let i = 0; i < 12; i++) {
            if (tstr === this.lowercaseMonths[i]) {
                ret.found = true;
                ret.match = this.months[i];
                ret.type = "full";
                break;
            } else if (tstr.startsWith(this.lowercaseMonthAbbrs[i]) && this.lowercaseMonths[i].startsWith(tstr)) {
                ret.found = true;
                ret.match = this.months[i];
                ret.type = "abbr";
                break;
            }
        }

        return ret;
    },

    // compute regex
    crgx(regex, str) {
        const res = _.deburr(str).match(regex);

        if (res === null) {
            return [];
        } else {
            return res;
        }
    },

    // contains a present string
    containsPresent(str) {
        if (!str) {
            return false;
        }

        if (this.presentRegex.exec(str)) {
            return true;
        }
        return false;
    },

    // convert 56 to 1956 and 03 to 2003
    convertYear(str) {
        const year = parseInt(str);
        if (year > 1900) {
            return year;
        }
        if (year > 50) {
            return `${year + 1900}`;
        }
        return `${year + 2000}`;
    },

    // generate date info from string of form 'January 2008'
    genDateInfo(str) {
        if (str === "Present") {
            return {
                text: "Present"
            };
        }

        const self = this;
        return {
            text: str,
            month: self.months.indexOf(str.split(" ")[0]) + 1,
            year: parseInt(str.split(" ")[1])
        };
    },

    // compare dates of format
    // {
    //     text: "January 2016",
    //     month: 1,
    //     year: 2016
    // }
    compareDate(a, b){
        if(a.text === "Present") {
            return 1;
        } else if (b.text === "Present") {
            return -1;
        }
        if(a.year > b.year) {
            return 1;
        } else if (a.year === b.year) {
            if (a.month > b.month) {
                return 1;
            } else {
                return -1;
            }
        }
        return -1;
    },

    // calculates date difference in months
    // dates are of format
    // {
    //     text: "January 2016",
    //     month: 1,
    //     year: 2016
    // }
    calcMonths(a, b) {
        const array = [a, b];
        array.sort(this.compareDate);
        if(array[1].text === "Present"){
            const d = new Date();
            array[1].month = d.getMonth() + 1;
            array[1].year = d.getFullYear();
        }

        const [before, latter] = array;
        const yearsDiff = latter.year - before.year;
        const monthsDiff = latter.month - before.month;

        return yearsDiff * 12 + monthsDiff;
    },

    // January 2001
    // Febuary 1999
    // March 2003
    // April 2000
    // May 1988
    // June 1960
    // July 1910
    // August 18
    // September 22
    // October 99
    // November 00
    // December 99
    // and all abbreviated versions
    style1(str) {
        const ret = {
            match: [],
            raw: []
        };

        const regex = /(\b\w{3,9}[^a-zA-Z0-9]*((?:[1]|[2])(?:[0]|[9]))*(?:[0-9]|[6-9])[0-9]\b)/g;
        const res = this.crgx(regex, str);

        res.forEach((cpatt) => {
            cpatt = cpatt.replace(/[^A-Z0-9]/ig, " ");
            const toks = _.compact(cpatt.split(" "));
            const check = this.parseMonth(toks[0]);
            if (check.match !== "") {
                ret.match.push(`${check.match} ${this.convertYear(toks[1])}`);
                ret.raw.push(cpatt);
            }
        });

        return ret;
    },

    // 06 2000
    // 09/2000
    // 12' /01
    // 1/-- 1955
    // 04   /13
    style2(str) {
        const ret = {
            match: [],
            raw: []
        };

        const regex = /(\b(?:[1-9]|[0-1][0-9])[^a-zA-Z0-9]*((?:[1]|[2])(?:[0]|[9]))*(?:[0-9]|[6-9])[0-9]\b)/g;
        const res = this.crgx(regex, str);

        res.forEach((cpatt) => {
            cpatt = cpatt.replace(/[^A-Z0-9]/ig, " ");
            const toks = _.compact(cpatt.split(" "));
            const check = this.parseMonth(toks[0]);
            if (check.match !== "") {
                ret.match.push(`${check.match} ${this.convertYear(toks[1])}`);
                ret.raw.push(cpatt);
            }
        });

        return ret;
    },

    // January 2001
    // Febuary 1999
    // March 2003
    // April 2000
    // May 1988
    // June 1960
    // July 1910
    // August 18
    // September 22
    // October 99
    // November 00
    // December 99
    // and all abbreviated versions
    parseDates(str) {
        const ret = {
            match: [],
            raw: [],
            info: []
        };

        const self = this;

        function addMatches(array, present = false) {
            array.forEach((cpatt) => {
                const symStrip = cpatt.replace(/[^A-Z0-9]/ig, " ");

                if (present) {
                    ret.match.push("Present");
                    ret.raw.push(symStrip);
                    ret.info.push(self.genDateInfo("Present"));
                } else {
                    const toks = _.compact(symStrip.split(" "));
                    const check = self.parseMonth(toks[0]);

                    if (check.found) {
                        ret.match.push(`${check.match} ${self.convertYear(toks[1])}`);
                        ret.raw.push(symStrip);
                        ret.info.push(self.genDateInfo(`${check.match} ${self.convertYear(toks[1])}`));
                    }
                }
            });
        };

        const res1 = this.crgx(this.namedMonthRegex, str);
        const res2 = this.crgx(this.numberedMonthRegex, str);
        const res3 = this.crgx(this.presentRegex, str);

        addMatches(res2);
        addMatches(res1);
        addMatches(res3, true);

        ret.info.sort(this.compareDate);

        return ret;
    }
};