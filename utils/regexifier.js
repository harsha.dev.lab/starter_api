module.exports = {
    regexify(str) {

    },

    lower(str) {
        
    },

    escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    },

    endsWithSymbol(str) {
        let symbols = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+']
        let symbol
        for (let i = 0; i < symbols.length; i++) {
            symbol = symbols[i]
            if (str.endsWith(symbol)) {
                return true
            }
        }
        return false
    },

    startsWithSymbol(str) {
        let symbols = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+']
        let symbol
        for (let i = 0; i < symbols.length; i++) {
            symbol = symbols[i]
            if (str.startsWith(symbol)) {
                return true
            }
        }
        return false
    },

    genBoundaries(str) {
        let nstr = ''
        if (this.startsWithSymbol(str)) {
            nstr = nstr + '\\B'
        } else {
            nstr = nstr + '\\b'
        }
        nstr = nstr + str
        if (this.endsWithSymbol(str)) {
            nstr = nstr + '\\B'
        } else {
            nstr = nstr + '\\b'
        }
        return nstr
    },

    regexify(str) {
        return this.genBoundaries(this.escapeRegExp(str));
    }
};