const mongoose = require("mongoose");

before(done => {
  mongoose.connect("mongodb://localhost/cous3_test");
  mongoose.connection.once("open", () => done()).on("error", error => {
    console.warn("Warning", error);
  });
});

beforeEach(done => {
  const {
    users,
    skills
  } = mongoose.connection.collections;
  users
    .drop()
    .then(() => users.ensureIndex({
      "geometry.coordinates": "2dsphere"
    }))
    .then(() => skills.drop())
    .then(() => done())
    .catch(() => done());
});