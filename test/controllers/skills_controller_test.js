const assert = require("assert");
const request = require("supertest");
const mongoose = require("mongoose");
const app = require("../../app");

const Skill = mongoose.model("skill");

describe("skill controller", () => {
    it("Post to /api/skills creates a new skill", done => {
        Skill.count().then(count => {
            request(app)
                .post("/api/skills")
                .send({
                    name: "Python"
                })
                .end((skill) => {
                    Skill.count().then(newCount => {
                        assert(count + 1 === newCount);
                        done();
                    });
                });
        });
    });

    it("Post to /api/skills requires an name", done => {
        request(app)
            .post("/api/skills")
            .send({})
            .end((err, res) => {
                assert(res.body.error);
                done();
            });
    });

    it("Delete to /api/skills/:id can delete a record", done => {
        const skill = new Skill({
            name: "c++"
        });

        skill.save().then(() => {
            // console.log(skill);
            request(app)
                .delete(`/api/skills/${skill._id}`)
                .end(() => {
                    Skill.count().then(count => {
                        // console.log(count);
                        assert(count === 0);
                        done();
                    });
                });
        });
    });

    it("Post to /api/skills/search to search records", done => {
        const skill1 = new Skill({
            name: "C++"
        });

        const skill2 = new Skill({
            name: "Os/PCL"
        });

        const searchTerm = "os/";

        skill1.save()
            .then(() => skill2.save())
            .then(() => {
                request(app)
                    .post("/api/skills/search")
                    .send({
                        term: searchTerm
                    })
                    .end((err, res) => {
                        const result = res.body;
                        // console.log(result);
                        assert(result[0].name === "Os/PCL");
                        assert(result.length === 1);
                        done();
                    });
            }).catch((err) => {
                console.log(err);
                done();
            });
    });
});