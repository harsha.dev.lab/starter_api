const assert = require("assert");
const request = require("supertest");
const mongoose = require("mongoose");
const app = require("../../app");

const User = mongoose.model("user");

describe("users controller", () => {
  it("Post to /api/users creates a new user", done => {
    User.count().then(count => {
      request(app)
        .post("/api/users")
        .send({
          username: "applepie",
          password: "applelover",
          name: "Katie Smith",
          email: "test@test.com"
        })
        .end(() => {
          User.count().then(newCount => {
            assert(count + 1 === newCount);
            done();
          });
        });
    });
  });

  it("Post to /api/users requires an email", done => {
    request(app)
      .post("/api/users")
      .send({})
      .end((err, res) => {
        assert(res.body.error);
        done();
      });
  });

  it("Delete to /api/users/:id can delete a record", done => {
    const user = new User({
      username: "applepie",
      password: "applelover",
      name: "Katie Smith",
      email: "test@test.com"
    });

    user.save().then(() => {
      request(app)
        .delete(`/api/users/${user._id}`)
        .end(() => {
          User.count().then(count => {
            assert(count === 0);
            done();
          });
        });
    });
  });

  it("Get to /api/users finds users in a location", done => {
    const seattleUser = new User({
      username: "applepie",
      password: "applelover",
      name: "Katie Smith",
      email: "seattle@test.com",
      geometry: { type: "Point", coordinates: [-122.4759902, 47.6147628] }
    });
    const miamiUser = new User({
      username: "applepie2",
      password: "applelover",
      name: "John Doe",
      email: "miami@test.com",
      geometry: { type: "Point", coordinates: [-80.2534507, 25.791581] }
    });

    Promise.all([seattleUser.save(), miamiUser.save()]).then(() => {
      request(app)
        .get("/api/users?lng=-80&lat=25")
        .end((err, response) => {
          assert(response.body.length === 1);
          assert(response.body[0].email === "miami@test.com");
          done();
        });
    });
  });

  it("Post login /api/users/login", done => {
    const user = new User({
      username: "clonesta",
      password: "Ark4nsaaVV",
      name: "Clone Sta",
      email: "clonesta@test.com"
    });

    user.save().then(() => {
      request(app)
        .post("/api/users/login")
        .send({
          username: "clonesta",
          password: "Ark4nsaaVV"
        })
        .end((err, res) => {
          assert(res.body.name === "Clone Sta");
          done();
        });
    });
  });
});
