const assert = require("assert");
const elasticsearch = require("elasticsearch");
const esconfig = require("../../settings/esconfig");
const query = require("../../utils/querybuilder");

const client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace"
});

before(done => {
    client.indices.create({
            index: "talentmap_test",
            body: esconfig
        })
        .then(() => done())
        .catch((error) => {
            console.warn("Warning", error);
        });
});

after(done => {
    client.indices.delete({
            index: "talentmap_test"
        })
        .then(() => done())
        .catch((error) => {
            console.warn(("ERROR in after", error));
        });
});

describe("Elasticsearch test", () => {
    it("Creates and deletes index", done => {
        client.indices.create({
                index: "test_index",
                body: {}
            })
            .then((response) => {
                assert(response.acknowledged);
                assert(response.index === "test_index");

                client.indices.delete({
                        index: "test_index"
                    })
                    .then((response) => {
                        assert(response.acknowledged);
                        done();
                    })
                    .catch((error) => {
                        assert(false);
                        done();
                    });
            })
            .catch((error) => {
                assert(false);
                done();
            });
    });

    it("Tests document indexing", done => {
        client.index({
                index: "talentmap_test",
                type: "doc",
                body: {
                    text: "hello world!"
                }
            })
            .then((response) => {
                const createdID = response._id;
                assert(response.result === "created");

                const params = {
                    index: "talentmap_test",
                    type: "doc",
                    id: createdID
                };

                client.get(params)
                    .then((response) => {
                        assert(response.found);
                        assert(response._source.text === "hello world!");
                        done();
                    })
                    .catch((error) => {
                        assert(false);
                        done();
                    });
            })
            .catch((error) => {
                console.log(error);
                assert(false);
                done();
            });
    });

    it("Tests document searching", done => {
        client.index({
                index: "talentmap_test",
                type: "doc",
                body: {
                    text: "I love working in Python, Perl, C++"
                }
            })
            .then((response) => {
                const createdID = response._id;
                assert(response.result === "created");

                const searchQuery = query.build({
                    terms: ["Perl,"]
                });
                console.log("SEARCH QUERY:", searchQuery);

                client.indices.refresh({
                        index: "talentmap_test"
                    })
                    .then(() => {
                        client.search({
                                index: "talentmap_test",
                                body: searchQuery
                            })
                            .then(response => {
                                console.log(response);
                                assert(response.hits.total === 1);
                                const [first] = response.hits.hits;
                                console.log(first);
                                console.log(first._id);
                                console.log(createdID);
                                assert(first._id === createdID);
                                done();
                            })
                            .catch(error => {
                                assert(false);
                                done();
                            });
                    }).catch(error => {
                        console.warn("Warning in refresh", error);
                        done();
                    });

                // client.search({
                //         index: "talentmap_test",
                //         body: searchQuery
                //     })
                //     .then(response => {
                //         console.log(response);
                //         assert(response.hits.total === 1);
                //         const [first] = response.hits.hits;
                //         console.log(first);
                //         console.log(first._id);
                //         console.log(createdID);
                //         assert(first._id === createdID);
                //         done();
                //     })
                //     .catch(error => {
                //         assert(false);
                //         done();
                //     });
            })
            .catch((error) => {
                console.log(error);
                assert(false);
                done();
            });
    });
});