const assert = require("assert");
const request = require("supertest");
const mongoose = require("mongoose");
const app = require("../../app");

// const Resume = mongoose.model("resume");

describe("resume controller", () => {
    xit("Post to /api/resumes creates a new resume", done => {
        Resume.count().then(count => {
            request(app)
                .post("/api/resumes")
                .send({
                    owner: "0x982",
                    text: "Java, c++, PYthON"
                })
                .end((resume) => {
                    Resume.count().then(newCount => {
                        assert(count + 1 === newCount);
                        done();
                    });
                });
        });
    });

    xit("Post to /api/resumes requires an owner", done => {
        request(app)
            .post("/api/resumes")
            .send({})
            .end((err, res) => {
                assert(res.body.error);
                done();
            });
    });

    xit("Delete to /api/resumes/:id can delete a record", done => {
        const resume = new Resume({
            owner: "cx9923x",
            text: "JaVA, MsSQL, Python, GoLang"
        });

        resume.save().then(() => {
            request(app)
                .delete(`/api/resumes/${resume._id}`)
                .end(() => {
                    Resume.count().then(count => {
                        assert(count === 0);
                        done();
                    });
                });
        });
    });

    xit("Post to /api/resumes/search to search records", done => {
        const resume1 = new Resume({
            owner: "cx9923x2",
            text: "JaVA, MsSQL, Python, GoLang"
        });

        const resume2 = new Resume({
            owner: "cx9923x2",
            text: "Visual Basic, c++, java",
            tags: ["pokEMON"],
            notes: ["Should have C# as well"],
            skills: [
                {
                    name: "mysql",
                    experience: 27
                }
            ],
            experience: 15
        });

        const searchTerms = [
            "visual",
            "C++",
            "POkeMon",
            "c#",
            "mySQL"
        ];

        resume1.save()
            .then(() => resume2.save())
            .then(() => {
                request(app)
                    .post("/api/resumes/search")
                    .send({
                        owner: "cx9923x2",
                        terms: searchTerms,
                        blacklisted: false,
                        starred: false,
                        experience: {
                            from: 5,
                            to: 15
                        }
                    })
                    .end((err, res) => {
                        const result = res.body;
                        console.log(result);
                        // console.log(result[0].skills[0]);
                        assert(result.length === 1);
                        assert(result[0].owner === "cx9923x2");
                        done();
                    });
            }).catch((err) => {
                console.log(err);
                done();
            });
    });
});