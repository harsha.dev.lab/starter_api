const assert = require("assert");
const textparser = require("../../utils/textparser");

describe("text parser", () => {
    it("Parses text properly", done => {
        textparser.extractText("/home/sriharsha/apis/cous3/testresumes/AartiChabra_PHP.pdf")
            .then((result) => {
                assert(result.success);
                done();
            })
            .catch((err) => {
                assert(false);
                done();
            });
    });
});