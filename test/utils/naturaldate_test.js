const assert = require("assert");
const naturaldate = require("../../utils/naturaldate");

describe("naturaldate", () => {
    it("Parses style1", done => {
        const parsed = naturaldate.style1("January'--  00 to March  2001 and Octob 09 Jul 1999");
        [d1, d2, d3, d4] = parsed.match;

        assert(d1 === "January 2000");
        assert(d2 === "March 2001");
        assert(d3 === "October 2009");
        assert(d4 === "July 1999");
        done();
    });

    it("Parses style2", done => {
        const parsed = naturaldate.style2("01/20 13' 2011 02/--2011");
        [d1, d2] = parsed.match;

        assert(d1 === "January 2020");
        assert(d2 === "Febuary 2011");
        done();
    });

    it("Parses all dates", done => {
        const parsed = naturaldate.parseDates("01/20 jan 2013 and oct' 99 to jui 2000 and marc 00 tilll' date");
        [d1, d2, d3, d4, d5] = parsed.info;

        assert(d1.text === "October 1999");
        assert(d2.text === "March 2000");
        assert(d3.text === "January 2013");
        assert(d4.text === "January 2020");
        assert(d5.text === "Present");
        done();
    });

    it("Calculates months", done => {
        const d1 = {
            text: "Febuary 2012",
            month: 2,
            year: 2012
        };

        const d2 = {
            text: "January 2014",
            month: 1,
            year: 2014
        };

        const d3 = {
            text: "April 2014",
            month: 4,
            year: 2014
        }

        const present = {
            text: "Present"
        };

        const diff1 = naturaldate.calcMonths(d2, d1);       
        const diff2 = naturaldate.calcMonths(present, d1);
        const diff3 = naturaldate.calcMonths(d3, d2);

        assert(diff1 === 23);
        assert(diff2 === 76);
        assert(diff3 === 3);

        done();
    });
});