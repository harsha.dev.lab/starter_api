// const elasticsearch = require("elasticsearch");
// // const regexifier = require("../utils/regexifier");

// const esClient = new elasticsearch.Client({
//   host: "localhost:9200",
//   log: "trace"
// });

// esClient.ping()
//   .then(() => console.log("ES cluster is swell :)"))
//   .catch((err) => console.log("ES cluster is down!", err));

const ResumesController = require("../controllers/resumes_controller");

const resumeController = new ResumesController();

const UsersController = require("../controllers/users_controller");
// const ResumesController = require("../controllers/resumes_controller_old");
const SkillsController = require("../controllers/skills_controller");

module.exports = app => {
  app.post("/api/users", UsersController.create);
  app.put("/api/users/:id", UsersController.edit);
  app.delete("/api/users/:id", UsersController.delete);
  app.get("/api/users", UsersController.index);
  app.post("/api/users/login", UsersController.login);

  // app.post("/api/resumes", ResumesController.create);
  // app.put("/api/resumes/:id", ResumesController.edit);
  // app.delete("/api/resumes/:id", ResumesController.delete);
  // app.post("/api/resumes/search", ResumesController.search);

  app.post("/api/skills", SkillsController.create);
  app.put("/api/skills/:id", SkillsController.edit);
  app.delete("/api/skills/:id", SkillsController.delete);
  app.post("/api/skills/search", SkillsController.search);
};